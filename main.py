"""
Проект для кластеризации обучающих выборок методом K-means

(с) Алейников А.А.

26.05.2022
"""
import re
import string

import nltk
from clickhouse_driver import Client
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from stop_words import get_stop_words

"""
Метод препроцессора для tfidf векторизатора

Аргументы:
line - строка для обработки
"""


def preprocessing(line):
    line = line.lower()
    line = re.sub(r"[{}]".format(string.punctuation), " ", line)
    return line


"""
Метод для чтения текстовых полей из базы данных

Аргументы:
count - количество патентов
"""


def readDatabase(count):
    f = open("/home/andrew/test/text", "w")
    f.write("")
    f.close()
    for i in range(1, count[0][0]):
        text = ""
        absract = client.execute("select patents.abstract from patents where patents.id == %d" % i)

        description = client.execute("select hdfs_decsription.description from hdfs_decsription where id == %d " % i)
        claim = client.execute(
            "select claim from hdfs_claim where id== %d" % i)
        name = client.execute("select patents.invention_title from patents where patents.id == %d" % i)
        try:
            text = text + name[0][0] + " "
        except:
            print("Name error")
        try:
            text = text + absract[0][0] + " "
        except:
            print("Abstract error")
        try:
            text = text + description[0][0] + " "
        except:
            print("Description error")
        try:
            text = text + claim[0][0] + " "
        except:
            print("Claim error")
        text = text + "\n\n"
        patterns = r'[0-9!#№$%&\'()*+,\./:;<=>?@\[\]^_`{|}~—\"-]+'
        text = re.sub(patterns, ' ', text.lower())
        f = open("/home/andrew/test/text", "a")
        f.write(text)
        f.close()


"""
Метод для подготовки данных к кластеризации

"""


def processingText():
    f = open("/home/andrew/test/text", "r")
    text = f.read()
    lemmatizer = WordNetLemmatizer()
    text = text.split("\n\n")[1:-1]
    count = 0
    f = open("/home/andrew/test/processed_text", "w")
    f.write("")
    f.close()
    for patent in text:
        newtext = ""
        patent = nltk.word_tokenize(patent)
        patent = [word.lower() for word in patent if word.isalpha()]
        a = []
        for token in patent:
            lemmetized_word = lemmatizer.lemmatize(token)
            a.append(lemmetized_word)
        patent = a

        stop_words = list(get_stop_words('en'))
        nltk_words = list(stopwords.words('english'))
        stop_words.extend(nltk_words)

        t = [w for w in patent if not w in stop_words]

        for word in t:
            newtext = newtext + word + " "
        newtext += "\n\n"

        f = open("/home/andrew/test/processed_text", "a")
        f.write(newtext)
        f.close()
        print(count)
        count += 1


"""
Точка входа приложения

"""

if __name__ == '__main__':
    nltk.download('punkt')
    nltk.download('wordnet')
    nltk.download('omw-1.4')
    nltk.download('stopwords')
    client = Client('localhost')

    client.execute("use test")
    count = client.execute("select count() from patents")

    debug = True
    N_clusters = 4

    if not debug:
        readDatabase(count)
        processingText()

    f = open("/home/andrew/test/processed_text", "r")
    text = f.read()
    newtext = text.split("\n\n")[1:-1]
    tfidf_vectorizer = TfidfVectorizer(preprocessor=preprocessing, max_df=0.70, min_df=40, stop_words='english')
    tfidf = tfidf_vectorizer.fit_transform(newtext)

    kmeans = KMeans(n_clusters=N_clusters).fit(tfidf)
    for i in range(N_clusters):
        print(kmeans.labels_.tolist().count(i))
